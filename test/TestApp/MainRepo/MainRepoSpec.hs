module TestApp.MainRepo.MainRepoSpec (spec) where

import           Baulig.Prelude

import           Test.Hspec

import           TestApp.MainRepo.SpecHook

import           Baulig.Utils.GitCommit

spec :: SpecWith MainRepoTest
spec = describe "Repository test" $ do
    it "can read from 'git' command" $ \test -> do
        validateCommitHash (mainRepoTestRawHash test) `shouldBe` True
    it "can get the root commit" $ \test -> do
        gitCommitHash (mainRepoTestCommit test)
            `shouldBe` commitHash (mainRepoTestRawHash test)
