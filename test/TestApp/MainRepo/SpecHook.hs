module TestApp.MainRepo.SpecHook
    ( MainRepoTest(..)
    , hook
    ) where

import           Baulig.Prelude

import           RIO.Process
import qualified RIO.Text                    as T

import           Test.Hspec

import           Baulig.Utils.GitCommit      (GitCommit)
import           Baulig.Utils.LocalPath
import           Baulig.Web.Engine.App
import           Baulig.Web.Engine.GitHelper

------------------------------------------------------------------------------
--- MainRepoTest
------------------------------------------------------------------------------
-- | Main Repository tests.
data MainRepoTest = MainRepoTest { mainRepoTestRoot    :: !AbsoluteLocalPath
                                 , mainRepoTestRawHash :: !Text
                                 , mainRepoTestCommit  :: !GitCommit
                                 }
    deriving stock (Eq
                  , Show)

getCommitHash :: MonadIO m => m Text
getCommitHash = asText <$> readProcessStdout_ "git log -1 --pretty=%H"
  where
    asText = T.strip . decodeUtf8Lenient . toStrictBytes

withMainRepo :: ActionWith MainRepoTest -> ActionWith App
withMainRepo func app =
    func =<< MainRepoTest root <$> getCommitHash <*> getAppRepoHead
  where
    root = app ^. appRootPath

    getAppRepoHead = runRIO app $ getRepositoryHead root

------------------------------------------------------------------------------
--- hook
------------------------------------------------------------------------------
hook :: SpecWith MainRepoTest -> SpecWith App
hook = aroundAllWith withMainRepo
