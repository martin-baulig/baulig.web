module SpecHook (hook) where

import           Baulig.Prelude

import           System.Directory

import           Test.Hspec

import           Baulig.Utils.LocalPath
import           Baulig.Web.Engine.App
import           Baulig.Web.Engine.AppSettings

hook :: SpecWith App -> Spec
hook = aroundAll withApp

withApp :: (App -> IO a) -> IO a
withApp func = do
    root <- absoluteRootPath <$> getCurrentDirectory

    runAppWith (defaultTestSettings True root) $ liftIO . func =<< view appL
