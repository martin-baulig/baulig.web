module Baulig.Web.Engine.App
    ( App
    , HasApp(..)
    , runApp
    , runAppWith
    ) where

import           Baulig.Prelude

import           RIO.Directory
import           RIO.Time

import           Baulig.Utils.Bottleneck
import           Baulig.Utils.GitCommit
import           Baulig.Utils.LocalPath
import           Baulig.Web.Engine.AppSettings
import           Baulig.Web.Engine.GitHelper

------------------------------------------------------------------------------
--- AppSetup
------------------------------------------------------------------------------
data App = App { _appSettings    :: !AppSettings
               , _appLogFunc     :: !LogFunc
               , _appRootPath    :: !AbsoluteLocalPath
               , _appCurrentTime :: !ZonedTime
               , _appRepoHead    :: !GitCommit
               }

instance Display App where
    display this = display $ this <:> _appSettings this <+> _appRootPath this
        <+> _appCurrentTime this <+> _appRepoHead this

instance HasLogFunc App where
    logFuncL = lens _appLogFunc (\x y -> x { _appLogFunc = y })

------------------------------------------------------------------------------
--- HasApp
------------------------------------------------------------------------------
class (HasAppSettings env, HasLogFunc env) => HasApp env where
    appL :: Lens' env App

    appDebug :: Lens' env Bool
    appDebug = appL . appSettings . appSettingsDebug

    appRootPath :: SimpleGetter env AbsoluteLocalPath
    appRootPath = appL . to _appRootPath

    appCurrentTime :: SimpleGetter env ZonedTime
    appCurrentTime = appL . to _appCurrentTime

    appRepoHead :: SimpleGetter env GitCommit
    appRepoHead = appL . to _appRepoHead

instance HasApp App where
    appL = id

instance HasAppSettings App where
    appSettings = lens _appSettings (\x y -> x { _appSettings = y })

------------------------------------------------------------------------------
--- App
------------------------------------------------------------------------------
defaultLogOptions :: AppSettings -> IO LogOptions
defaultLogOptions settings = do
    logOptions <- logOptionsHandle stderr True
    pure $ setLogMinLevel (view appSettingsLogLevel settings)
        $ setLogUseTime False $ setLogUseLoc False
        $ setLogSecondaryColor "\ESC[38;5;245m" logOptions

runApp :: RIO App () -> IO ()
runApp inner = flip runAppWith inner =<< parseCommandLineArguments

runAppWith :: AppSettings -> RIO App x -> IO x
runAppWith settings inner = do
    logOptions <- defaultLogOptions settings
    cwd <- getCurrentDirectory

    rootPath <- absoluteRootPath <$> canonicalizePath cwd

    zonedTime <- getZonedTime

    withBottleneckLogger logOptions $ \logFunc -> do
        head <- runRIO logFunc $ getRepositoryHead rootPath
        flip runRIO inner $ App settings logFunc rootPath zonedTime head
