module Baulig.Web.Engine.DebugHelper
    ( displayObjOid
    , displayTreeEntry
    ) where

import           Baulig.Prelude

import           Data.Tagged

import           Git

displayObjOid :: IsOid (Oid r) => ObjectOid r -> Utf8Builder
displayObjOid obj = case obj of
    BlobObjOid oid -> inner "TagObjOid" oid
    TreeObjOid oid -> inner "TreeObjOid" oid
    CommitObjOid oid -> inner "CommitObjOid" oid
    TagObjOid oid -> inner "TagObjOid" oid
  where
    inner :: IsOid o => Utf8Builder -> Tagged r o -> Utf8Builder
    inner kind oid =
        mconcat ["[", kind, " ", displayShow (renderOid $ untag oid), "]"]

displayTreeEntry :: IsOid (Oid r) => TreeEntry r -> Utf8Builder
displayTreeEntry entry = case entry of
    CommitEntry oid -> inner "CommitEntry" oid
    BlobEntry oid PlainBlob -> inner "PlainBlobEntry" oid
    BlobEntry oid kind -> inner ("BlobEntry " <> displayShow kind) oid
    TreeEntry oid -> inner "TreeEntry" oid
  where
    inner :: IsOid o => Utf8Builder -> Tagged r o -> Utf8Builder
    inner kind oid =
        mconcat ["[", kind, " ", displayShow (renderOid $ untag oid), "]"]
