module Baulig.Web.Engine.GitException (GitException(..)) where

import           Baulig.Prelude

import           Baulig.Utils.GitCommit

data GitException = GitException (Maybe CommitHash) Text
    deriving stock (Show)

instance Display GitException where
    display (GitException commit text) =
        display $ "GitException" <:=!> commit <+> text

instance Exception GitException
