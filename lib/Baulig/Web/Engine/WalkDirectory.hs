module Baulig.Web.Engine.WalkDirectory
    ( FileNameKind(..)
    , FileNameFilter
    , FileNameScanFunc
    , WalkDirectoryMap
    , collectFiles
    , walkDirectory
    ) where

import           Baulig.Prelude

import           RIO.Directory
import qualified RIO.HashMap            as HM

import           Baulig.Utils.LocalPath

------------------------------------------------------------------------------
--- walkDirectory
------------------------------------------------------------------------------
type WalkDirectoryMap x = HM.HashMap AbsoluteLocalPath x

-- | Whether this is a file or a directory.
data FileNameKind = FileKind | DirectoryKind

{-|
  File name filter function.
-}
type FileNameFilter = FileNameKind -> AbsoluteLocalPath -> Bool

type FileNameScanFunc m x =
    WalkDirectoryMap x -> AbsoluteLocalPath -> m (WalkDirectoryMap x)

{-|
  Walk directory hierarchy, populating and returning a 'WalkDirectoryMap'.
-}
walkDirectory
    :: HasLogFunc env
    => Maybe FileNameFilter  -- ^ Optional file name filter.  Return False to skip the given file / directory.
    -> FileNameScanFunc (RIO env) x
    -- ^ Scanning function.  This is a Monadic fold over the 'WalkDirectoryMap'.
    -> WalkDirectoryMap x -- ^ Previous 'WalkDirectoryMap'.
    -> AbsoluteLocalPath -- ^ Start directory.
    -> RIO env (WalkDirectoryMap x)
walkDirectory maybeFilter scanFunc map' directory = do
    logDebug $ "Scan directory: " <> display directory

    let filterFunc = fromMaybe (\_ _ -> True) maybeFilter

    entries <- listDirectory $ absoluteFilePath directory
    let fullPath = entries <&> \entry -> directory </> relativeLocalPath entry

    directories <- filter (filterFunc DirectoryKind)
        <$> filterM (doesDirectoryExist . absoluteFilePath) fullPath
    files <- filter (filterFunc FileKind)
        <$> filterM (doesFileExist . absoluteFilePath) fullPath

    forM_ directories $ \dir -> logDebug $ "Directories: " <> display dir
    forM_ files $ \file -> logDebug $ "Files: " <> display file

    newMap <- foldM (walkDirectory maybeFilter scanFunc) map' directories

    foldM scanFunc newMap files

collectFiles :: HasLogFunc env
             => Maybe (AbsoluteLocalPath -> Bool)
             -> [AbsoluteLocalPath]
             -> AbsoluteLocalPath
             -> RIO env [AbsoluteLocalPath]
collectFiles maybeFilter list directory = do
    logDebug $ "Scan directory: " <> display directory

    entries <- listDirectory $ absoluteFilePath directory
    let fullPath = entries <&> \entry -> directory </> relativeLocalPath entry
    directories <- filterM (doesDirectoryExist . absoluteFilePath) fullPath
    files <- filterM (doesFileExist . absoluteFilePath) fullPath

    forM_ directories $ \dir -> logDebug $ "Directories: " <> display dir
    forM_ files $ \file -> logDebug $ "Files: " <> display file

    newList <- foldM (collectFiles maybeFilter) list directories

    let filterFunc = fromMaybe (\_ -> True) maybeFilter
    let filtered = filter filterFunc files

    logDebug $ "Filtered files: " <> (displayShow filtered)

    pure $ newList <> filtered
