module Baulig.Web.Engine.GitHelper
    ( CommitHash
    , getRepositoryHead
    , newScanRepository
    ) where

import           Baulig.Prelude

import           RIO.FilePath                   (isExtensionOf)
import qualified RIO.HashMap                    as HM

import           Conduit

import           Data.Tagged

import           Git
import qualified Git.Libgit2                    as LG

import           Baulig.Utils.GitCommit
import           Baulig.Utils.LocalPath
import           Baulig.Web.Engine.GitException

repositoryOptions :: AbsoluteLocalPath -> RepositoryOptions
repositoryOptions (absoluteFilePath . absolutePathRoot -> root) =
    RepositoryOptions { repoPath       = root
                      , repoWorkingDir = Just root
                      , repoIsBare     = False
                      , repoAutoCreate = False
                      }

runWithRepository
    :: MonadUnliftIO m
    => RepositoryOptions
    -> ((m () -> ReaderT LG.LgRepo IO ()) -> ReaderT LG.LgRepo IO b)
    -> m b
runWithRepository options func = withRunInIO
    $ \runInIO -> withRepository' LG.lgFactory options $ func $ liftIO . runInIO

getRepositoryHead :: HasLogFunc env => AbsoluteLocalPath -> RIO env GitCommit
getRepositoryHead path = do
    logDebug $ "Get repository head: " <> display path
    runWithRepository (repositoryOptions path) $ \io -> do
        fromGitLibCommit <$> resolveCommitOrThrow io "HEAD"

resolveCommitOrThrow :: (HasLogFunc env, LG.HasLgRepo m, LG.MonadLg m)
                     => (RIO env () -> m ())
                     -> RefName
                     -> m (Commit LG.LgRepo)
resolveCommitOrThrow io refName = do
    io $ logDebug $ "resolveCommitOrThrow: " <> display refName
    ref <- lookupReference refName
    io $ case ref of
        Just (RefSymbolic sym) ->
            logDebug $ "Got symbolic reference: " <> displayShow sym
        Just (RefObj oid) -> logDebug $ "Got OID reference: " <> displayShow oid
        Nothing -> logDebug "No reference."
    commit <- resolveCommitInternal "HEAD"
    case commit of
        Nothing -> throwIO $ GitException Nothing
            $ "Failed to resolve '" <> refName <> "' commit."
        Just commit' -> pure commit'

resolveCommitInternal :: (LG.HasLgRepo m, LG.MonadLg m)
                      => RefName
                      -> m (Maybe (Commit LG.LgRepo))
resolveCommitInternal name = do
    ref <- resolveReference name
    case ref of
        Nothing -> pure Nothing
        Just ref' -> do
            obj <- lookupObject ref'
            pure $ case obj of
                CommitObj commitObj -> Just commitObj
                _ -> Nothing

newScanRepository :: HasLogFunc env => AbsoluteLocalPath -> RIO env ()
newScanRepository path = do
    logDebug $ "Get repository head: " <> display path
    runWithRepository (repositoryOptions path) $ \io -> do
        commit <- resolveCommitOrThrow io "HEAD"

        io $ logDebug $ "GOT COMMIT: " <> displayShow (commitOid commit) <> " "
            <> displayShow (commitTree commit)

        out <- runConduit $ sourceObjects Nothing (commitOid commit) False
            .| mapTreeObjects path .| sinkList

        let hash = foldl' buildHash mempty out

        io $ do
            logDebug $ "GOT TREE DONE: " <> displayShow (commitTree commit)

            forM_ (HM.toList hash) $ \(key, value) ->
                logDebug $ "ENTRY: " <> display key <> " - " <> display value

            logDebug $ "DONE: " <> displayShow hash
  where
    buildHash :: TreeMap -> [MyTreeEntry] -> TreeMap
    buildHash = foldl' $ \this entry ->
        HM.insertWith update (mtePath entry) entry this
      where
        update new old
            | mteBlobOid new == mteBlobOid old = old
        update new _ = new

mapTreeObjects :: MonadGit r m
               => AbsoluteLocalPath
               -> ConduitT (ObjectOid r) [MyTreeEntry] m ()
mapTreeObjects root = awaitForever inner
  where
    inner (CommitObjOid oid) = do
        commit <- lift $ lookupCommit oid
        tree <- lift $ lookupTree $ commitTree commit

        let hash = commitHash $ renderOid $ untag oid

        yield =<< (lift $ runConduit $ mapTreeEntries root hash tree)

    inner _ = pure ()

mapTreeEntries :: MonadGit r m
               => AbsoluteLocalPath
               -> CommitHash
               -> Tree r
               -> ConduitT a c m [MyTreeEntry]
mapTreeEntries root hash tree =
    mapOutputMaybe (filterFile . mapPlainBlob hash root)
                   (sourceTreeEntries tree) .| sinkList

mapPlainBlob :: IsOid (Oid r)
             => CommitHash
             -> AbsoluteLocalPath
             -> (TreeFilePath, TreeEntry r)
             -> Maybe MyTreeEntry
mapPlainBlob commit root (path, BlobEntry oid PlainBlob) = Just
    $ MyTreeEntry commit (root </> decodeUtf8Lenient path) $ renderOid
    $ untag oid
mapPlainBlob _ _ _ = Nothing

isAnyExtensionOf :: HasRelativePath x => [Text] -> x -> Bool
isAnyExtensionOf (fmap txt2str -> exts) (relativeFilePath -> path) =
    any (flip isExtensionOf path) exts

filterFile :: Maybe MyTreeEntry -> Maybe MyTreeEntry
filterFile (Just entry)
    | isAnyExtensionOf ["org", "md", "yaml"] $ mtePath entry = Just entry
filterFile _ = Nothing

type TreeMap = HM.HashMap AbsoluteLocalPath MyTreeEntry

data MyTreeEntry = MyTreeEntry { mteCommit  :: !CommitHash
                               , mtePath    :: !AbsoluteLocalPath
                               , mteBlobOid :: !Text
                               }
    deriving stock (Eq)

instance Display MyTreeEntry where
    display this =
        display $ this <:> mteCommit this <+> mtePath this <+> mteBlobOid this

instance Show MyTreeEntry where
    show = txt2str . textDisplay
