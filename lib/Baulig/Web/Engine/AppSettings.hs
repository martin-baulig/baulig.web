module Baulig.Web.Engine.AppSettings
    ( AppSettings
    , HasAppSettings(..)
    , defaultTestSettings
    , parseCommandLineArguments
    ) where

import           Baulig.Prelude

import           System.Console.ParseArgs
import           System.Directory

import           Baulig.Utils.LocalPath

------------------------------------------------------------------------------
--- AppSettings
------------------------------------------------------------------------------
data AppSettings = AppSettings { asLogLevel :: !LogLevel
                               , asDebug    :: !Bool
                               , asRootDir  :: !AbsoluteLocalPath
                               }
    deriving stock (Eq)

instance Display AppSettings where
    display settings = display $ settings <:!> asLogLevel settings
        <+!> asDebug settings <+> asRootDir settings

------------------------------------------------------------------------------
--- HasAppSettings
------------------------------------------------------------------------------
class HasAppSettings x where
    appSettings :: Lens' x AppSettings

    appSettingsLogLevel :: Lens' x LogLevel
    appSettingsLogLevel = appSettings
        . lens asLogLevel (\x y -> x { asLogLevel = y })

    appSettingsDebug :: Lens' x Bool
    appSettingsDebug = appSettings . lens asDebug (\x y -> x { asDebug = y })

    appSettingsRootDir :: SimpleGetter x AbsoluteLocalPath
    appSettingsRootDir = appSettings . to asRootDir

instance HasAppSettings AppSettings where
    appSettings = id

-----------------------------------------------------------------------------
-- parseCommandLineArguments
-----------------------------------------------------------------------------
data Options = OptionVerbose | OptionDebug | OptionRootDir
    deriving stock (Eq
                  , Show
                  , Ord)

argDescription :: [Arg Options]
argDescription =
    [ Arg { argIndex = OptionVerbose
          , argName  = Just "verbose"
          , argAbbr  = Just 'v'
          , argData  = Nothing
          , argDesc  = "Verbose logging."
          }
    , Arg { argIndex = OptionDebug
          , argName  = Just "debug"
          , argAbbr  = Just 'd'
          , argData  = Nothing
          , argDesc  = "Enable debugging."
          }
    , Arg { argIndex = OptionRootDir
          , argName  = Just "root"
          , argAbbr  = Just 'r'
          , argData  = argDataOptional "path" ArgtypeString
          , argDesc  = "Root directory."
          }
    ]

parseCommandLineArguments :: MonadIO m => m AppSettings
parseCommandLineArguments = do
    args <- liftIO $ parseArgsIO (ArgsParseControl ArgsComplete ArgsHardDash)
                                 argDescription

    current <- liftIO getCurrentDirectory

    let logLevel = bool LevelInfo LevelDebug $ gotArg args OptionVerbose
    let debug = gotArg args OptionDebug
    let root = fromMaybe current $ getArg args OptionRootDir

    pure $ AppSettings logLevel debug $ absoluteRootPath root

defaultTestSettings :: Bool -> AbsoluteLocalPath -> AppSettings
defaultTestSettings debug = AppSettings (bool LevelInfo LevelDebug debug) debug
