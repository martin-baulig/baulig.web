module Main (main) where

import           Baulig.Prelude

import           Baulig.Web.Engine.App
import           Baulig.Web.Engine.GitHelper

main :: IO ()
main = runApp $ do
    logInfo "Test app."
    newScanRepository =<< view appRootPath
